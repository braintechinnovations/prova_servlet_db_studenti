package com.lezione26.hw.studenti;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;

import com.lezione26.hw.studenti.classi.Studente;
import com.lezione26.hw.studenti.connessione.GestisciStudenti;

@WebServlet("/salvastudente")
public class SalvataggioServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Errore di reperimento dati o richiamo tramite GET");
		response.sendRedirect("index.html");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String var_nome = request.getParameter("input_nome");
		String var_cognome = request.getParameter("input_cognome");
		String var_matricola = request.getParameter("input_matricola");
		String var_sesso = request.getParameter("select_sesso");
		
		if(!var_nome.isEmpty() && !var_cognome.isEmpty() && !var_sesso.isEmpty()) {
			
			Studente stud = new Studente();
			stud.setNome(var_nome);
			stud.setCognome(var_cognome);
			stud.setMatricola(var_matricola);
			stud.setSesso(var_sesso);
			
			GestisciStudenti gestore = new GestisciStudenti();
			try {
				gestore.insert(stud);
//				PrintWriter out = response.getWriter();
//				out.println("Inserito con successo");
				
//				HttpSession sessione = request.getSession();
//				sessione.setAttribute("stud_id", stud.getId());
//				sessione.setAttribute("stud_nome", stud.getNome());
//				sessione.setAttribute("stud_cognome", stud.getCognome());
//				sessione.setAttribute("stud_matricola", stud.getMatricola());
//				sessione.setAttribute("stud_sesso", stud.getSesso());
				
				response.sendRedirect("dettaglio_studente.jsp?id_studente=" + stud.getId());
				
			} catch (SQLException e) {
				System.out.println("ERRORE DI SALVATAGGIO: " + e.getMessage());
//				e.printStackTrace();
			}
			
			
		}
		else {
			doGet(request, response);
		}
		
	}

}

package com.lezione26.hw.studenti.classi;

public class Studente {

	private int id;
	private String nome;
	private String cognome;
	private String matricola;
	private String sesso;
	
	public Studente(){
		
	}
	
	public Studente(String nome, String cognome, String matricola, String sesso) {
		super();
		this.nome = nome;
		this.cognome = cognome;
		this.matricola = matricola;
		this.sesso = sesso;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCognome() {
		return cognome;
	}
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}
	public String getMatricola() {
		return matricola;
	}
	public void setMatricola(String matricola) {
		this.matricola = matricola;
	}
	public String getSesso() {
		return sesso;
	}
	public void setSesso(String sesso) {
		this.sesso = sesso;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
}

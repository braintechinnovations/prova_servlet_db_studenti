package com.lezione26.hw.studenti.connessione;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.lezione26.hw.studenti.classi.Studente;

public class GestisciStudenti {

	public boolean insert(Studente obj_stud) throws SQLException {
		Connection conn = ConnettoreDB.getIstance().getConnection();
		
		String query = "INSERT INTO studente (nome, cognome, matricola, sesso) VALUES (?,?,?,?)";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
		ps.setString(1, obj_stud.getNome());
		ps.setString(2, obj_stud.getCognome());
		ps.setString(3, obj_stud.getMatricola());
		ps.setString(4, obj_stud.getSesso());
		
		ps.executeUpdate();
		ResultSet risultato = ps.getGeneratedKeys();
		risultato.next();
		
		obj_stud.setId(risultato.getInt(1));
		return true;
	}
	
	public ArrayList<Studente> findAll() throws SQLException{
		
		ArrayList<Studente> elenco = new ArrayList<Studente>();
		Connection conn = ConnettoreDB.getIstance().getConnection();
		
		String query = "SELECT studenteid, nome, cognome, matricola, sesso FROM studente";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		
		ResultSet risultato = ps.executeQuery();
		while(risultato.next()) {
			Studente stud_temp = new Studente();
			stud_temp.setId(risultato.getInt(1));
			stud_temp.setNome(risultato.getString(2));
			stud_temp.setCognome(risultato.getString(3));
			stud_temp.setMatricola(risultato.getString(4));
			stud_temp.setSesso(risultato.getString(5));
			
			elenco.add(stud_temp);
		}
		
		return elenco;		
	}
	
	public Studente findById(int var_id)  throws SQLException{

		Connection conn = ConnettoreDB.getIstance().getConnection();

		String query = "SELECT studenteid, nome, cognome, matricola, sesso FROM studente WHERE studenteid = ?";
		PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
		ps.setInt(1, var_id);
		
		ResultSet risultato = ps.executeQuery();
		risultato.next();
		
		Studente stud_temp = new Studente();
		stud_temp.setId(risultato.getInt(1));
		stud_temp.setNome(risultato.getString(2));
		stud_temp.setCognome(risultato.getString(3));
		stud_temp.setMatricola(risultato.getString(4));
		stud_temp.setSesso(risultato.getString(5));
		
		return stud_temp;
		
	}
	
}

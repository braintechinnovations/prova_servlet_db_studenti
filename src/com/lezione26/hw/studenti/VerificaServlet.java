package com.lezione26.hw.studenti;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VerificaServlet
 */
@WebServlet("/inseriscistudente")
public class VerificaServlet extends HttpServlet {       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.println("METODO NON PERMESSO");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String var_nome = request.getParameter("input_nome");
		String var_cognome = request.getParameter("input_cognome");
		String var_matricola = request.getParameter("input_matricola");
		String var_sesso = request.getParameter("select_sesso");

//		String totale = var_nome + "\n" + 
//				var_cognome + "\n" + 
//				var_matricola + "\n" + 
//				var_sesso;
//
//		PrintWriter out = response.getWriter();
//		out.println(totale);
		
		PrintWriter out = response.getWriter();
		if(var_nome.isEmpty()) {
			out.println("ERRORE NOME");
			return;
		}
		if(var_cognome.isEmpty()) {
			out.println("ERRORE COGNOME");
			return;
		}
		if(var_sesso.isEmpty()) {
			out.println("ERRORE SESSO");
			return;
		}
		
		//Invio i parametri ad un'altra Servlet che si occuper� del salvataggio
		RequestDispatcher rd = request.getRequestDispatcher("salvastudente");
		rd.forward(request, response);
		
	}

}

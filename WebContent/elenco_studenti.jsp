<%@page import="com.lezione26.hw.studenti.classi.Studente"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.lezione26.hw.studenti.connessione.GestisciStudenti"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>Elenco Studenti</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col">
				<table class="table">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Cognome</th>
							<th>Matricola</th>
							<th>Sesso</th>
							<th>Azioni</th>
						</tr>
					</thead>
					<tbody>
					
						<%
						
							ArrayList<Studente> elenco = new ArrayList<Studente>();
						
							GestisciStudenti gestore = new GestisciStudenti();
							elenco = gestore.findAll();
							
							String riga_risultante = "";
							
							for(int i=0; i<elenco.size(); i++){
								Studente temp = elenco.get(i);
								
								riga_risultante += "<tr>";
									riga_risultante += "<td>" + temp.getNome() + "</td>";
									riga_risultante += "<td>" + temp.getCognome() + "</td>";
									riga_risultante += "<td>" + temp.getMatricola() + "</td>";
									riga_risultante += "<td>" + temp.getSesso() + "</td>";
									riga_risultante += "<td><a href='dettaglio_studente.jsp?id_studente=" + temp.getId() + "'>Dettaglio</a></td>";
								riga_risultante += "</tr>";
							}
							
							out.print(riga_risultante);
							
						%>
					
					</tbody>
				</table>
			</div>
		</div>		
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>
<%@page import="com.lezione26.hw.studenti.classi.Studente"%>
<%@page import="com.lezione26.hw.studenti.connessione.GestisciStudenti"%>
<%@page import="java.io.PrintWriter"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<title>Dettaglio Studente</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6 text-center">
			
				<%
					int var_id = Integer.parseInt(request.getParameter("id_studente"));

					GestisciStudenti gestore = new GestisciStudenti();
					Studente stud_selezionato = gestore.findById(var_id);
				%>
				
				<form action="inseriscistudente" method="POST">
					<h1>Dettaglio Studente</h1>
					<div class="form-group">
						Nome: <% out.print(stud_selezionato.getNome()); %>
					</div>
					<div class="form-group">
						Cognome: <% out.print(stud_selezionato.getCognome()); %>
					</div>
					<div class="form-group">
						Matricola: <% out.print(stud_selezionato.getMatricola()); %>
					</div>
					
					<div class="form-group">
						Sesso:  <% out.print(stud_selezionato.getSesso()); %>
					</div>
				</form>
			</div>
			<div class="col-md-3"></div>	
		</div>
	
		<%-- <div class="row">
			<div class="col">
				<%
					/* String nome = "Giovanni";
				
					out.print(nome); */
					
					/* HttpSession sessione = request.getSession();
			
					out.print(sessione.getAttribute("stud_id"));
					out.print(sessione.getAttribute("stud_nome"));
					out.print(sessione.getAttribute("stud_cognome"));
					out.print(sessione.getAttribute("stud_matricola"));
					out.print(sessione.getAttribute("stud_sesso")); */
					
					
				%>
				
				
			</div>
		</div>	 --%>	
	</div>

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>